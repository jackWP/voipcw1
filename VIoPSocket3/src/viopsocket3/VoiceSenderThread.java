/**
 * Program : VoiceSenderThread
 * Date : 01/03/2019
 * Version : 1
 * Description : Class for starting a UDP VoIP sender thread
 * @author ajc17dru
 */
package viopsocket3;

import java.net.*;
import java.io.*;
import CMPC3M06.AudioRecorder;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import uk.ac.uea.cmp.voip.*;

public class VoiceSenderThread implements Runnable
{

    /**
     * USed for sending data through the chosen datagram socket
     */
    static private DatagramSocket3 sending_socket3;
    
    /**
     * Stores the senders IP address
     */
    private static String IP;
    

    public void start(String s) 
    {
        IP = s;
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() 
    {
        //IP ADDRESS to send to
        int PORT = 55555;
        InetAddress clientIP = null;
        
        //try to open a connection
        try 
        {
            clientIP = InetAddress.getByName(IP);
        } 
        catch (UnknownHostException e) 
        {
            System.out.println("ERROR: VoiceSender: Could not find client IP");
            e.printStackTrace();
            System.exit(0);
        }
        
        //open socket
        try 
        {
            sending_socket3 = new DatagramSocket3();
        }
        catch (SocketException e) 
        {
            System.out.println("ERROR: VoiceSender: Could not open UDP "
                    + "socket to send from.");
            e.printStackTrace();
            System.exit(0);
        }

        // Audio recorder
        AudioRecorder recorder = null;
        try 
        {
            recorder = new AudioRecorder();
        } 
        catch (LineUnavailableException ex) 
        {
            Logger.getLogger(
                 VoiceSenderThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        short sequence = 0;
        
        ArrayList<byte[]> list = new ArrayList<>();
        //Main loop.             
        while (VoIPClient.stop == false) 
        {
            try 
            {
                // Get audio block
                byte[] block = recorder.getBlock();               
                
                //Use XOR with bit rotations to encrypt
                ByteBuffer voipBlock = ByteBuffer.allocate(block.length);
                
                if(VoIPClient.compensation == true)
                {
                    //reset sequence counter
                    if(sequence == 9) sequence =0;
                    //crc code to determine if packet was corrupted
                    
                    //encrypted data
                    ByteBuffer buffer = ByteBuffer.allocate(514);

                    //add sequnce number
                    buffer.putShort(sequence);
                    //add data
                    buffer.put(voipBlock.array());
                    
                    list.add(buffer.array());
                    
                   // increment sequence counter
                    sequence++;
                    
                    if(sequence == 9)
                    {
                        //send filled buffer
                        for(byte[] b : list)
                        {
                            DatagramPacket packet = new DatagramPacket
                            (b, b.length, clientIP, PORT);
                            //Send encrypted packet
                            sending_socket3.send(packet);
                        }
                        //clear list for next loop
                        list.clear();
                        //reset counters to 0 when it reaches max buffer value
                        sequence = 0;
                    }
                }       
            } 
            catch (IOException e) 
            {
                System.out.println("ERROR: VoiceSender: IO error occured!");
                e.printStackTrace();
            }
        }
        //Close the socket
        sending_socket3.close();
        recorder.close();   //Close audio input
    }
}
