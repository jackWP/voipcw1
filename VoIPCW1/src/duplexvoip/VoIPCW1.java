/** 
 * Program : Main class for VoIP program
 * Date : 01/03/2019
 * Version : 1
 * Description : Provides Main for VoIP program. Once executed, Main thread is 
 *               not needed as VoIPClient thread handles functionality
 * @author ajc17dru
 */

package duplexvoip;

public class VoIPCW1 {

    public static void main(String[] args) 
    {
        VoIPClient client = new VoIPClient();       
        client.start();
  
    }
}
