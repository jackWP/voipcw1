/** 
 * Program : VoiceSenderThread
 * Date : 01/03/2019
 * Version : 1
 * Description : Class for starting a UDP VoIP receiver thread
 * @author ajc17dru
 */

package duplexvoip;

import java.io.*;
import java.net.*;
import CMPC3M06.AudioPlayer;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import uk.ac.uea.cmp.voip.*;

public class VoiceReceiverThread implements Runnable
{
    /**
     * Used to receive data through the chosen datagram socket
     */
    static private DatagramSocket receiving_socket;
    
    /**
     * authentication key made from senders ip address
     */
    static private short authKey;
    
    /**
     * Start thread with given key
     * @param k Authentication key to use
     */
    public void start(short k)
    {
        authKey = k;
        Thread thread = new Thread(this);
        thread.start();
    }

    /**
     * Run and start VoiceReceiverThread 
     */
    @Override
    public void run() 
    {
        //Port to open socket on
        int PORT = 55555;

        try
        {
            receiving_socket = new DatagramSocket2(PORT);
	} 
        catch (SocketException e)
        {
            System.out.println("VoiceReceiver: Could not open UDP socket "
                    + "to receive from.");
            e.printStackTrace();
            System.exit(0);
	}
        
        // Audio player
        AudioPlayer player = null;
        try 
        {
            player = new AudioPlayer();
        } 
        catch (LineUnavailableException ex) 
        {
            Logger.getLogger(VoiceReceiverThread.class.getName()
                                                  ).log(Level.SEVERE, null, ex);
        }
        //counter for undoing interleaving
        int counter = 0;
        //list to store blocks of audio data
        byte[][] playList = new byte[9][];
        
        //copy of last received packet tro play if packet missed
        byte[] previousBlock = new byte[512];
        
        //Main loop.      
        while (VoIPClient.stop == false)
        {
            try 
            {
                //Receive a DatagramPacket 
                byte[] encryptedBlock = new byte[518];
                DatagramPacket packet = 
                                    new DatagramPacket(encryptedBlock, 0, 518);

                receiving_socket.setSoTimeout(500);
                receiving_socket.receive(packet);                              

                ByteBuffer packetBuffer = ByteBuffer.wrap(encryptedBlock);
                
                if(VoIPClient.compensation == true)
                {
                    short authKeyRec = packetBuffer.getShort();   
                    System.out.println("read autkey " + authKeyRec);
                                                               
                    short recCRC = packetBuffer.getShort();
                    System.out.println("read recCRC " + recCRC);
                    
                    short sequence = packetBuffer.getShort(); 
                    System.out.println("read sequnce " + sequence);
                    
                    //Decrypt and play audio in voipBlock if authKey matches
                    if(authKey == authKeyRec)
                    {
                        //get encrypted VoiP data block
                        byte[] voipBlock = new byte[packetBuffer.remaining()];
                        packetBuffer.get(voipBlock);
                       
                        //wrap encrypted data
                        ByteBuffer voipBlockBuffer = ByteBuffer.wrap(voipBlock);
                        
                        //allocate buffer for decrypted data
                        ByteBuffer decryptedBuffer = ByteBuffer.allocate(512);
                        
                        //caluclate crc packet and compare to received one
                        short crc = 0;
                        //decrypt block into buffer
                        for(int i = 0; i < voipBlock.length/4; i++) 
                        {
                            int fourByte = voipBlockBuffer.getInt();
                            //circular shift bits left sequence number positions
                            Integer.rotateLeft(sequence, fourByte); //--------------------FIND GOOD CONSTANT FOR BIT SHIFTING
                            //undo XOR
                            fourByte ^= authKey;
                            crc+= fourByte;
                            decryptedBuffer.putInt(fourByte); 
                        }
                        crc += authKey;

                        if(crc == recCRC)
                        {
                            if(counter < 9 && playList[sequence] == null)
                            {
                                playList[sequence] = decryptedBuffer.array();
                                previousBlock = decryptedBuffer.array();
                            }
                            /**
                             * Once list is filled or duplicate index found 
                             * meaning packet has been missed, play buffer of audio
                             * filling in gaps with previous received packet
                             */
                            else
                            {
                                for(int i = 0; i < 9; i++)
                                {
                                    //if buffer sequnce location os empty, play 
                                    //last received packet
                                    if(playList[i] == null)
                                    {
                                        playList[i] = previousBlock;
                                    }
                                    //else play packet in location
                                    else
                                    {
                                        player.playBlock(playList[i]);
                                    }
                                }
                                //reset counter for next filling of the buffer
                                counter = 0;     
                                //reset buffer for next playloop
                                Arrays.fill(playList, null);
                            }
                        }
                        else
                        {
                            System.out.println("CRC codes did not match, "
                                    + "caluclated " + crc + " got "+ recCRC);
                        }
                    }
                    else
                    {
                        System.out.println("Packet recieved from non-matching "
                                + "authentication key source: " + authKeyRec + 
                                " expected key: " + authKey);
                    }  
                }
                //if no compensatons used, play unmodified block
                else
                {
                    player.playBlock(encryptedBlock);
                }
            } 
            catch (SocketTimeoutException e)
            {
                System.out.printf(".");
            }
            catch (IOException e)
            {
                System.out.println("ERROR: An IO error occured.");
                e.printStackTrace();
            }
        }
        //Close the socket
        receiving_socket.close();
    }
}