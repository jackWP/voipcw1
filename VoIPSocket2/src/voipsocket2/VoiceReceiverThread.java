/** 
 * Program : VoiceSenderThread
 * Date : 01/03/2019
 * Version : 1
 * Description : Class for starting a UDP VoIP receiver thread on socket 2
 * @author ajc17dru
 */

package voipsocket2;

import java.io.*;
import java.net.*;
import CMPC3M06.AudioPlayer;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import uk.ac.uea.cmp.voip.*;

public class VoiceReceiverThread implements Runnable
{
    /**
     * Used to receive data through the chosen datagram socket
     */
    static private DatagramSocket2 receiving_socket2;
    
    public void start()
    {
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() 
    {
        //Port to open socket on
        int PORT = 55555;

        try
        {
            receiving_socket2 = new DatagramSocket2(PORT);
	} 
        catch (SocketException e)
        {
            System.out.println("VoiceReceiver: Could not open UDP socket "
                    + "to receive from.");
            e.printStackTrace();
            System.exit(0);
	}
        
        // Audio player
        AudioPlayer player = null;
        try 
        {
            player = new AudioPlayer();
        } 
        catch (LineUnavailableException ex) 
        {
            Logger.getLogger(VoiceReceiverThread.class.getName()
                                                  ).log(Level.SEVERE, null, ex);
        }
        
        int counter = 0;
        //list to store blocks of audio data
        byte[][] playList = new byte[9][];
        
        //copy of last received packet to play if packet missed
        byte[] previousBlock = new byte[512];
        
        int i = 0;
        int j = 0;

        //Main loop.      
        while (VoIPClient.stop == false)
        {
            try 
            {
                //Receive a DatagramPacket 
                byte[] packetBlock = new byte[512];
                DatagramPacket packet = 
                                    new DatagramPacket(packetBlock, 0, 512);

                receiving_socket2.setSoTimeout(500);
                receiving_socket2.receive(packet);                              
                
                //make copy for missing blocks
                previousBlock = packetBlock;
                
                if(VoIPClient.compensation == true)
                {
                   // int pos = j*3 +(3-1-i);
                    if(i == 3)
                    {
                        i = 0;
                        j++;
                    }
                    //when end of column
                    if(j == 3)
                    {
                        i = 0;
                        j = 0;
                    }
                    
                    int pos = ((j + i) + 3) %9;
                    System.out.println("pos = " + pos);
                    i++;
                    
                    if (counter == 9 || playList[pos] != null)
                    {
                        //play block
                        counter = i = j = 0;
                        
                        for(byte[] b : playList)
                        {
                            if(b == null)
                            {
                                player.playBlock(previousBlock);
                            }
                            else
                            {
                                player.playBlock(b);
                            }
                        }
                    }
                    else
                    {
                         playList[pos] = packetBlock;
                    }
                }
                //if no compensatons used, play unmodified block
                else
                {
                    player.playBlock(packetBlock);
                }
            } 
            catch (SocketTimeoutException e)
            {
                System.out.printf(".");
            }
            catch (IOException e)
            {
                System.out.println("ERROR: An IO error occured.");
                e.printStackTrace();
            }
        }
        //Close the socket
        receiving_socket2.close();
        player.close();
    }
}