/**
 * Program : VoiceSenderThread
 * Date : 01/03/2019
 * Version : 1
 * Description : Class for starting a UDP VoIP sender thread
 * @author ajc17dru
 */
package voipsocket4;

import java.net.*;
import java.io.*;
import CMPC3M06.AudioRecorder;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import uk.ac.uea.cmp.voip.*;

public class VoiceSenderThread implements Runnable
{

    /**
     * USed for sending data through the chosen datagram socket
     */
    static private DatagramSocket4 sending_socket4;
    
    /**
     * Stores the senders IP address
     */
    private static String IP;
    
    /**
     * Authentication key made from receivers IP address
     */
    private static short authKey;

    public void start(String s,short k) 
    {
        IP = s;
        authKey = k;
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() 
    {
        //IP ADDRESS to send to
        int PORT = 55555;
        InetAddress clientIP = null;
        
        //try to open a connection
        try 
        {
            clientIP = InetAddress.getByName(IP);
        } 
        catch (UnknownHostException e) 
        {
            System.out.println("ERROR: VoiceSender: Could not find client IP");
            e.printStackTrace();
            System.exit(0);
        }
        
        //open socket
        try 
        {
            sending_socket4 = new DatagramSocket4();
        }
        catch (SocketException e) 
        {
            System.out.println("ERROR: VoiceSender: Could not open UDP "
                    + "socket to send from.");
            e.printStackTrace();
            System.exit(0);
        }

        // Audio recorder
        AudioRecorder recorder = null;
        try 
        {
            recorder = new AudioRecorder();
        } 
        catch (LineUnavailableException ex) 
        {
            Logger.getLogger(
                 VoiceSenderThread.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Main loop.             
        while (VoIPClient.stop == false) 
        {
            try 
            {
                // Get audio block
                byte[] block = recorder.getBlock();               
                
                ByteBuffer unwrapEncrypt = ByteBuffer.allocate(block.length);

                ByteBuffer plainText = ByteBuffer.wrap(block);
                
                if(VoIPClient.compensation == true)
                {
                    //crc code to determine if packet was corrupted
                    short crc = 0;
                    //encrypted data
                    ByteBuffer buffer = ByteBuffer.allocate(516);
                    //apply XOR to data

                    for (int l = 0; l < block.length / 4; l++) 
                    {
                        int fourByte = plainText.getInt();
                        crc += fourByte;
                        fourByte ^= authKey;

                        //circular shift right by authKey positions
                        Integer.rotateRight(authKey, fourByte);

                        unwrapEncrypt.putInt(fourByte);
                    }
                    
                    crc += authKey;
                    
                    //add to key header
                    buffer.putShort(authKey);
                    //add crc code
                    buffer.putShort(crc);

                    //add data
                    buffer.put(unwrapEncrypt.array());

                    //make packet from encrypted data
                    DatagramPacket packet = new DatagramPacket(buffer.array(),
                    buffer.array().length, clientIP, PORT);

                    //Send encrypted packet
                    sending_socket4.send(packet);
                }       

                //Make a DatagramPacket with unmodified data
                DatagramPacket packet = new DatagramPacket(block,
                        block.length, clientIP, PORT);

                //Send unmodified packet
                sending_socket4.send(packet);
            } 
            catch (IOException e) 
            {
                System.out.println("ERROR: VoiceSender: IO error occured!");
                e.printStackTrace();
            }
        }
        //Close the socket
        sending_socket4.close();
        recorder.close();   //Close audio input
    }
}
