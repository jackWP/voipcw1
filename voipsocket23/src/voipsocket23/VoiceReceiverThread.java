/** 
 * Program : VoiceSenderThread
 * Date : 01/03/2019
 * Version : 1
 * Description : Class for starting a UDP VoIP receiver thread
 * @author ajc17dru
 */

package voipsocket23;

import java.io.*;
import java.net.*;
import CMPC3M06.AudioPlayer;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import uk.ac.uea.cmp.voip.*;

public class VoiceReceiverThread implements Runnable
{
    /**
     * Used to receive data through the chosen datagram socket
     */
    static private DatagramSocket3 receiving_socket2;
    
    /**
     * Start thread with given key
     */
    public void start()
    {
        Thread thread = new Thread(this);
        thread.start();
    }

    /**
     * Run and start VoiceReceiverThread 
     */
    @Override
    public void run() 
    {
        //Port to open socket on
        int PORT = 55555;

        try
        {
            receiving_socket2 = new DatagramSocket3(PORT);
	} 
        catch (SocketException e)
        {
            System.out.println("VoiceReceiver: Could not open UDP socket "
                    + "to receive from.");
            e.printStackTrace();
            System.exit(0);
	}
        
        // Audio player
        AudioPlayer player = null;
        try 
        {
            player = new AudioPlayer();
        } 
        catch (LineUnavailableException ex) 
        {
            Logger.getLogger(VoiceReceiverThread.class.getName()
                                                  ).log(Level.SEVERE, null, ex);
        }
        //counter for undoing interleaving
        int counter = 0;
        //list to store blocks of audio data
        byte[][] playList = new byte[9][];
        
        //copy of last received packet tro play if packet missed
        byte[] previousBlock = new byte[512];
        
        //Main loop.      
        while (VoIPClient.stop == false)
        {
            try 
            {
                //Receive a DatagramPacket 
                byte[] dataBlock = new byte[514];
                DatagramPacket packet = 
                                    new DatagramPacket(dataBlock, 0, 514);

                receiving_socket2.setSoTimeout(500);
                receiving_socket2.receive(packet);                              

                ByteBuffer packetBuffer = ByteBuffer.wrap(dataBlock);
                
                if(VoIPClient.compensation == true)
                {                  
                    short sequence = packetBuffer.getShort(); 

                    //get encrypted VoiP data block
                    byte[] voipBlock = new byte[packetBuffer.remaining()];
                    packetBuffer.get(voipBlock);

                    /**
                     * Once list is filled or duplicate index found 
                     * meaning packet has been missed, play buffer of audio
                     * filling in gaps with previous received packet
                     */
                    
                    if(counter == 9 || playList[sequence] != null)
                    {
                        for(byte[] b : playList)
                        {
                            if(b == null)
                            {
                                player.playBlock(previousBlock);
                            }
                            else
                            {
                                player.playBlock(b);
                            }
                        }
                        counter = 0;
                        Arrays.fill(playList, null);
                    }
                    else
                    {
                        playList[sequence] = voipBlock;
                        previousBlock = voipBlock;
                        counter++;
                    }
                }
                //if no compensatons used, play unmodified block
                else
                {
                    player.playBlock(dataBlock);
                }
            } 
            catch (SocketTimeoutException e)
            {
                System.out.printf(".");
            }
            catch (IOException e)
            {
                System.out.println("ERROR: An IO error occured.");
                e.printStackTrace();
            }
        }
        //Close the socket
        receiving_socket2.close();
        player.close();
    }
}