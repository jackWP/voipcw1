/**
 * Program : VoiceSenderThread
 * Date : 01/03/2019
 * Version : 1
 * Description : Class for starting a UDP VoIP sender thread on socket 2
 * @author ajc17dru
 */
package voipsocket23;

import java.net.*;
import java.io.*;
import CMPC3M06.AudioRecorder;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import uk.ac.uea.cmp.voip.*;

public class VoiceSenderThread implements Runnable
{

    /**
     * USed for sending data through the chosen datagram socket
     */
    static private DatagramSocket3 sending_socket2;
    
    /**
     * Stores the senders IP address
     */
    private static String IP;

    public void start(String s) 
    {
        IP = s;
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() 
    {
        //IP ADDRESS to send to
        int PORT = 55555;
        InetAddress clientIP = null;
        
        //try to open a connection
        try 
        {
            clientIP = InetAddress.getByName(IP);
        } 
        catch (UnknownHostException e) 
        {
            System.out.println("ERROR: VoiceSender: Could not find client IP");
            e.printStackTrace();
            System.exit(0);
        }
        
        //open socket
        try 
        {
            sending_socket2 = new DatagramSocket3();
        }
        catch (SocketException e) 
        {
            System.out.println("ERROR: VoiceSender: Could not open UDP "
                    + "socket to send from.");
            e.printStackTrace();
            System.exit(0);
        }

        // Audio recorder
        AudioRecorder recorder = null;
        try 
        {
            recorder = new AudioRecorder();
        } 
        catch (LineUnavailableException ex) 
        {
            Logger.getLogger(
                 VoiceSenderThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        short sequence = 0;
        
        byte[][] playList = new byte[9][];
        int j = 0;
        int i = 0;
        //Main loop.             
        while (VoIPClient.stop == false) 
        {
            try 
            {
                // Get audio block
                byte[] block = recorder.getBlock();
                
                if(VoIPClient.compensation == true)
                {
                    //when end of row
                    if(i == 3)
                    {
                        i = 0;
                        j++;
                    }
                    //when end of column
                    if(j == 3)
                    {
                        i = 0;
                        j = 0;
                    }
                    //reset sequence counter
                    if(sequence == 9)
                    {
                        //send filled buffer
                        for(byte[] b : playList)
                        {
                            DatagramPacket leafedPacket = new DatagramPacket
                            (b, b.length, clientIP, PORT);
                            //Send encrypted packet
                            sending_socket2.send(leafedPacket);
                        }
                        //reset counters to 0 when it reaches max interleaver value
                        sequence = 0;
                        i = 0;
                        j= 0;
                         //clear list for next loop
                        Arrays.fill(playList, null);
                    }
                    else
                    {
                        int pos = (j*3) +(3 - 1 - i); //broke---------------------------------------------
                        System.out.println(pos);
                        
                        ByteBuffer buffer = ByteBuffer.allocate(514);
                        buffer.putShort(sequence);
                        buffer.put(block);
                        
                        //ADD SEQUNCE NUMBER HERE
                        playList[pos] = buffer.array();
                        sequence++;// increment sequence counter
                        i++;
                    }
                }
                else
                {
                    DatagramPacket packet = new DatagramPacket(block,
                                                block.length, clientIP, PORT);
                    sending_socket2.send(packet);
                }
            } 
            catch (IOException e) 
            {
                System.out.println("ERROR: VoiceSender: IO error occured!");
                e.printStackTrace();
            }
        }
        //Close the socket
        sending_socket2.close();
        recorder.close();   //Close audio input
    }
}
