/** 
 * Class : VoIPClient
 * Date : 01/03/2019
 * Version : 1
 * Description : Provides wrapper functionality for sender and receiver class 
 *               threads. Allows for starting and stopping of program by user
 * @author ajc17dru
 */

package voipsocket23;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class VoIPClient implements Runnable
{
    /**
     * Boolean used to stop the program when the user wants to
     */
    public volatile static boolean stop = false;
    
    /**
     * Determines whether network error compensation is used
     */
    public volatile static boolean compensation = false;

    public void start()
    {
       Thread thread = new Thread(this);
       thread.start();
    }

    @Override
    public void run() 
    {
       //reader for reading in input from console

       BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

       //string for user input IP/PC name
       String ip = "";
       //get user input for IP/PC name
       while(ip.equals(""))
       {
           System.out.println("Please type the IP address or PC name to "
                   + "connect to (use 'l' to connect to localhost):");
           try
           {
               ip = br.readLine();
               
               if(ip.equals("l"))
               {
                   ip = InetAddress.getLocalHost().getHostAddress();
                   break;
               }
           }
           catch(IOException e)
           {
               System.out.println("IO exception: " + e);
           }
       }
       
       System.out.println("Attempting to connect to: " + ip);
       //initilaise sender and reciever threads
       VoiceSenderThread sender = new VoiceSenderThread();
       VoiceReceiverThread receive = new VoiceReceiverThread();
       
       //key to use on sender thread
       short sendAuthKey = 0;
       
       //get local ip address to use as sender authentication key
       try
       {
            sendAuthKey = (short) InetAddress.getLocalHost()
                                  .getHostAddress().hashCode();         
       }
       catch(UnknownHostException e)
       {
           System.out.println("Error getting IP: " + e);
       }
       
       String s = "";
       //ask use if they want to use network compensation
       while(!("y".equals(s) || "n".equals(s)))
       {
           System.out.println("Do you want to use network compensation? y/n");
           
           try
           {
               s = br.readLine();
               
               if(s.equals("n"))
               {
                   compensation = false;
               }
               else if(s.equals("y"))
               {
                   compensation = true;
               }
           }
           catch (IOException e)
           {
               System.out.println("IO exception: " + e);
           }
       }

       //check if key was made successfully
       if (sendAuthKey != 0)
       {    //start threads with needed information
            receive.start();
            sender.start(ip);

            //user can exit program by typing 'e'
            //use ip variable instead of making a new one
            while(!ip.equals("e"))
            {
                 System.out.println("Type 'e' to exit the program...");
                 try
                 {
                     ip = br.readLine();
                 }
                 catch(IOException e)
                 {
                     System.out.println(e);
                 }
            }

            //exit program and stop sender and receiver threads
            stop = true;
       }
    }
}