/**
 * Program : voipanalysisrec
 * Date : 21/02/2019
 * Version : 1
 * Description : Class for receiving basic UDP packets
 * @author ajc17dru
 */
package voipanalysis;

import java.net.*;
import java.io.*;
import uk.ac.uea.cmp.voip.DatagramSocket2;
import uk.ac.uea.cmp.voip.DatagramSocket3;
import uk.ac.uea.cmp.voip.DatagramSocket4;

public class VoIPAnalysisRATERec 
{
    
    static DatagramSocket receiving_socket;
    static private DatagramSocket2 receiving_socket2;
    static private DatagramSocket3 receiving_socket3;
    static private DatagramSocket4 receiving_socket4;
    
    public static void main (String[] args)
    {
     
        int PORT = 55555;
        
        try
        {
            receiving_socket4 = new DatagramSocket4(PORT);
	} 
        catch (SocketException e)
        {
            System.out.println("ERROR: TextReceiver: Could not open UDP socket to receive from.");
            e.printStackTrace();
            System.exit(0);
	}
        boolean running = true;
        int c = 0;
        long t = System.currentTimeMillis();
        long end = System.currentTimeMillis() + 30000;
        while (t < end)
        {
            try
            {
                byte[] buffer = new byte[0];
                DatagramPacket packet = new DatagramPacket(buffer, 0, 0);

                receiving_socket4.receive(packet);
                c++;
                System.out.println("test");
                t = System.currentTimeMillis();
            } 
            catch (IOException e)
            {
                System.out.println("ERROR: TextReceiver: Some random IO error occured!");
                e.printStackTrace();
            }
        }
        System.out.println(c);

        receiving_socket4.close();
    }
}
