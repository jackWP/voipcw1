/**
 * Program : VoIPAnalysisDELAYRec
 * Date : 09/03/2019
 * Version : 1
 * Description : Class for receiving delay testing packets over UDP sockets
 * @author ajc17dru
 */
package voipanalysis;

import java.net.*;
import java.io.*;
import java.nio.ByteBuffer;
import uk.ac.uea.cmp.voip.DatagramSocket2;
import uk.ac.uea.cmp.voip.DatagramSocket3;
import uk.ac.uea.cmp.voip.DatagramSocket4;

public class VoIPAnalysisDELAYRec 
{
    
    static DatagramSocket receiving_socket;
    static private DatagramSocket2 receiving_socket2;
    static private DatagramSocket3 receiving_socket3;
    static private DatagramSocket4 receiving_socket4;
    
    public static void main (String[] args)
    {
     
        int PORT = 55555;
        
        try
        {
            receiving_socket4 = new DatagramSocket4(PORT);
	} 
        catch (SocketException e)
        {
            System.out.println("ERROR: TextReceiver: Could not open UDP socket to receive from.");
            e.printStackTrace();
            System.exit(0);
	}
        int c = 0;    
        long s = 0;
        while (c < 1000 )
        {
            try
            {
                ByteBuffer b = ByteBuffer.allocate(Long.BYTES);
                byte[] buffer = new byte[Long.BYTES];
                DatagramPacket packet = new DatagramPacket(buffer, 0, Long.BYTES);

                receiving_socket4.receive(packet);
                                
                b.put(buffer);
                b.flip();
                long t = System.currentTimeMillis();

                long e = b.getLong();
                s += t - e ;
                System.out.println("got " + e);
                System.out.println("time now " + t);
                                

                System.out.println("current total " + s);
                
                c++;

            } 
            catch (IOException e)
            {
                System.out.println("ERROR: TextReceiver: Some random IO error occured!");
                e.printStackTrace();
            }
        }
        System.out.println("average " + s/1000);

        receiving_socket4.close();
    }
}
