/**
 * Program : VoIPAnalysisDELAYSend
 * Date : 09/03/2019
 * Version : 1
 * Description : Class for sending delay testing packets over UDP sockets
 * @author ajc17dru
 */
package voipanalysis;

import java.net.*;
import java.io.*;
import java.nio.ByteBuffer;
import uk.ac.uea.cmp.voip.DatagramSocket2;
import uk.ac.uea.cmp.voip.DatagramSocket3;
import uk.ac.uea.cmp.voip.DatagramSocket4;

public class VoIPAnalysisDELAYSend {
    
    static DatagramSocket sending_socket;
    static private DatagramSocket2 sending_socket2;
    static private DatagramSocket3 sending_socket3;
    static private DatagramSocket4 sending_socket4;
    
    public static void main (String[] args){     

        int PORT = 55555;

        InetAddress clientIP = null;
	try 
        {
		clientIP = InetAddress.getByName("localhost");
	} 
        catch (UnknownHostException e) 
        {
            System.out.println("ERROR: TextSender: Could not find client IP");
            e.printStackTrace();
            System.exit(0);
	}

        try
        {
            sending_socket4 = new DatagramSocket4();
	} 
        catch (SocketException e)
        {
            System.out.println("ERROR: TextSender: Could not open UDP socket to send from.");
            e.printStackTrace();
            System.exit(0);
	}
        
        boolean running = true;
        
        while (running){
            try
            {
                long t = System.currentTimeMillis();
                ByteBuffer b = ByteBuffer.allocate(Long.BYTES);
                b.putLong(t);
                byte[] buffer = b.array();

                DatagramPacket packet = 
                new DatagramPacket(buffer, buffer.length, clientIP, PORT);
                
                sending_socket4.send(packet);

            }
            catch (IOException e)
            {
                System.out.println
                ("ERROR: TextSender: Some random IO error occured!");
                e.printStackTrace();
            }
        }
        sending_socket4.close();
    }
} 
